<?php

$totalFiles = 0;
$readFails  = array();
$writeFails = array();


function replace($path, $newContent)
{
	global $totalFiles;

	var_dump($path);

	$handle = fopen($path, "w");

	if(!$handle)
		return false;

	if(!fwrite($handle, $newContent))
		return false;

	@fclose($handle);

	$totalFiles++;
	return true;
}

function refactor($path, $str, $newStr)
{
	global $readFails, $writeFails;

	$filesList  = scandir($path);
	$totalFiles = sizeof($filesList); 

	for($i = 2; $i < $totalFiles; $i++)
	{
		if($filesList[$i] != 'refactor.php' && $filesList[$i] != '.' && $filesList[$i] != '..')
		{
			$fileName = $path.$filesList[$i];

			if(is_dir($fileName))
				refactor($fileName.'/', $str, $newStr);
			else 
			{
				$handle = @fopen($fileName, "r");

				if(!$handle)
					$readFails[] = $fileName;
				else
				{
					if(($contents = @fread($handle, filesize($fileName))))
					{
						if(strpos($contents, $str))
						{
							if(!replace($fileName, str_replace($str, $newStr, $contents)))
								$writeFails[] = $fileName;
						}
					}

					@fclose($handle);
				}
				
			}
		}

		
	}
}


refactor(getcwd().'\\',  'acr.browser.lightning', 'com.adblock.browser');

echo "Completed! - Total replaced files: {$totalFiles}, Read fails: \r\n";
var_dump($readFails);

echo "\r\n\r\nWrite fails: \r\n";
var_dump($writeFails);
?>