/*
 * Copyright 2014 A.C.R. Development
 */
package com.adblock.browser.controller;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Message;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient.CustomViewCallback;

import com.adblock.browser.model.SettingsItem;
import com.adblock.browser.activity.TabsManager;
import com.adblock.browser.database.HistoryItem;
import com.adblock.browser.view.LightningView;
import com.adblock.browser.view.SettingsItemsManager;

public interface UIController {

    void changeToolbarBackground(@NonNull Bitmap favicon, @Nullable Drawable drawable);

    @ColorInt
    int getUiColor();

    boolean getUseDarkTheme();

    void updateUrl(@Nullable String title, boolean shortUrl);

    void updateProgress(int n);

    void updateHistory(@Nullable String title, @NonNull String url);

    void openFileChooser(ValueCallback<Uri> uploadMsg);

    void onShowCustomView(View view, CustomViewCallback callback);

    void onShowCustomView(View view, CustomViewCallback callback, int requestedOrienation);

    void onHideCustomView();

    void onCreateWindow(Message resultMsg);

    void onCloseWindow(LightningView view);

    void hideActionBar();

    void showActionBar();

    void showFileChooser(ValueCallback<Uri[]> filePathCallback);

    void closeEmptyTab();

    void showCloseDialog(int position);

    void newTabButtonClicked();

    void newIncognitoButtonClicked();

    void tabCloseClicked(int position);

    void tabClicked(int position);

    void newTabButtonLongClicked();

    void bookmarkButtonClicked();

    void bookmarkItemClicked(@NonNull String url);

    void closeSettingsDrawer();

    void closeTabsDrawer();

    void setForwardButtonEnabled(boolean enabled);

    void setBackButtonEnabled(boolean enabled);

    void tabChanged(LightningView tab);

    void hideHomepage(LightningView tab);

    void closeBrowser(boolean finish);

    TabsManager getTabModel();

    void onBackButtonPressed();

    void onForwardButtonPressed();

    void onHomeButtonPressed();

    void onSettingsItemClicked(SettingsItem item);

    void onSettingsItemChecked(SettingsItem item, boolean isChecked);

    void onSettingsTopBarItemClicked(SettingsItemsManager.SETTINGS_ITEM_BAR_TYPE settingsItemBarType);

}
