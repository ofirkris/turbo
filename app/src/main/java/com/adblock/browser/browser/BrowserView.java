package com.adblock.browser.browser;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.View;

public interface BrowserView {

    void setTabView(@NonNull View view);

    void removeTabView();

    void updateUrl(String url, boolean shortUrl);

    void updateProgress(int progress);

    void updateTabNumber(int number);

    void closeBrowser(boolean finish );

    void closeActivity();

    void showBlockedLocalFileDialog(DialogInterface.OnClickListener listener);

    void showSnackbar(@StringRes int resource);

    void setForwardButtonEnabled(boolean enabled);

    void setBackButtonEnabled(boolean canWebViewGoBack);

    void notifyTabViewRemoved(int position);

    void notifyTabViewAdded();

    void notifyTabViewChanged(int position);

    void notifyTabViewInitialized();

}
