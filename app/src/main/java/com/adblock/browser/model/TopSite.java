package com.adblock.browser.model;

/**
 * Created by Mickael on 3/8/2017.
 */

public class TopSite {
    private String title;
    private String bgColor;
    private String drawableName;
    private String siteUrl;

    public TopSite(String siteName, String title, String bgColor, String drawableName, String siteUrl) {
        this.title = siteName;
        this.title = title;
        this.bgColor = bgColor;
        this.drawableName = drawableName;
        this.siteUrl = siteUrl;
    }

    public String getSiteName() {
        return title;
    }

    public void setSiteName(String siteName) {
        this.title = siteName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getDrawableName() {
        return drawableName;
    }

    public void setDrawableName(String drawableName) {
        this.drawableName = drawableName;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }
}
