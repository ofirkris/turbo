package com.adblock.browser.model;

import android.graphics.Bitmap;

import com.adblock.browser.R;

/**
 * Created by Mickael on 3/6/2017.
 */

public class SettingsItem {
    private String itemName;
    private Bitmap itemIcon;

    private boolean isCheckable;
    private boolean isChecked = false;

    private ITEM_TYPE itemType;

    public SettingsItem(String itemName, Bitmap itemIcon, boolean isCheckable, ITEM_TYPE item_type) {
        this.itemName = itemName;
        this.itemIcon = itemIcon;
        this.isCheckable = isCheckable;
        this.itemType = item_type;

    }

    public ITEM_TYPE getItemType() {
        return itemType;
    }

    public void setItemType(ITEM_TYPE itemType) {
        this.itemType = itemType;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Bitmap getItemIcon() {
        return itemIcon;
    }

    public void setItemIcon(Bitmap itemIcon) {
        this.itemIcon = itemIcon;
    }

    public boolean isCheckable() {
        return isCheckable;
    }

    public void setCheckable(boolean checkable) {
        isCheckable = checkable;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public enum ITEM_TYPE {
        AD_BLOCK,
        NEW_TAB,
        NEW_INCOGNITO_TAB,
        SHARE,
        OPEN_BOOKMARKS,
        OPEN_HISTORY,
        FIND_IN_PAGE,
        COPY_LINK,
        READER_MODE,
        REQUEST_DESKTOP_SITE,
        ADD_TO_HOME_SCREEN,
        OPEN_SETTINGS,
    }

    public static ITEM_TYPE inferItemType(int stringResId)
    {
        switch (stringResId) {
//            case android.R.id.home:
//                if (mDrawerLayout.isDrawerOpen(getBookmarkDrawer())) {
//                    mDrawerLayout.closeDrawer(getBookmarkDrawer());
//                }
//                return true;
//            case R.id.action_back:
//                if (currentView != null && currentView.canGoBack()) {
//                    currentView.goBack();
//                }
//                return true;
//            case R.id.action_forward:
//                if (currentView != null && currentView.canGoForward()) {
//                    currentView.goForward();
//                }
//                return true;
            case R.string.block_ads:
                return ITEM_TYPE.AD_BLOCK;
            case R.string.action_desktop_site:
                return ITEM_TYPE.REQUEST_DESKTOP_SITE;
            case R.string.action_add_to_homescreen:
                return ITEM_TYPE.ADD_TO_HOME_SCREEN;
            case R.string.action_new_tab:
                return ITEM_TYPE.NEW_TAB;
            case R.string.action_incognito:
                return ITEM_TYPE.NEW_INCOGNITO_TAB;
            case R.string.action_share:
                return ITEM_TYPE.SHARE;
            case R.string.action_bookmarks:
                return ITEM_TYPE.OPEN_BOOKMARKS;
            case R.string.action_copy:
                return ITEM_TYPE.COPY_LINK;
            case R.string.settings:
                return ITEM_TYPE.OPEN_SETTINGS;
            case R.string.action_history:
                return ITEM_TYPE.OPEN_HISTORY;
            case R.string.action_find:
                return ITEM_TYPE.FIND_IN_PAGE;
            case R.string.reading_mode:
                return ITEM_TYPE.READER_MODE;
        }
        return null;
    }



}
