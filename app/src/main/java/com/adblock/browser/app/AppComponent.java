package com.adblock.browser.app;

import javax.inject.Singleton;

import com.adblock.browser.activity.BrowserActivity;
import com.adblock.browser.activity.ReadingActivity;
import com.adblock.browser.activity.TabsManager;
import com.adblock.browser.activity.ThemableBrowserActivity;
import com.adblock.browser.activity.ThemableSettingsActivity;
import com.adblock.browser.browser.BrowserPresenter;
import com.adblock.browser.constant.StartPage;
import com.adblock.browser.dialog.LightningDialogBuilder;
import com.adblock.browser.download.LightningDownloadListener;
import com.adblock.browser.fragment.BookmarkSettingsFragment;
import com.adblock.browser.fragment.BookmarksFragment;
import com.adblock.browser.fragment.DebugSettingsFragment;
import com.adblock.browser.fragment.LightningPreferenceFragment;
import com.adblock.browser.fragment.PrivacySettingsFragment;
import com.adblock.browser.fragment.SettingsFragment;
import com.adblock.browser.fragment.TabsFragment;
import com.adblock.browser.fragment.TopSitesFragment;
import com.adblock.browser.search.SuggestionsAdapter;
import com.adblock.browser.utils.AdBlock;
import com.adblock.browser.utils.ProxyUtils;
import com.adblock.browser.view.LightningView;
import com.adblock.browser.view.LightningWebClient;
import com.adblock.browser.view.SettingsItemsManager;
import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(BrowserActivity activity);

    void inject(BookmarksFragment fragment);

    void inject(SettingsFragment fragment);

    void inject(TopSitesFragment fragment);

    void inject(SettingsItemsManager settingsItemsManager);

    void inject(BookmarkSettingsFragment fragment);

    void inject(LightningDialogBuilder builder);

    void inject(TabsFragment fragment);

    void inject(LightningView lightningView);

    void inject(ThemableBrowserActivity activity);

    void inject(LightningPreferenceFragment fragment);

    void inject(BrowserApp app);

    void inject(ProxyUtils proxyUtils);

    void inject(ReadingActivity activity);

    void inject(LightningWebClient webClient);

    void inject(ThemableSettingsActivity activity);

    void inject(AdBlock adBlock);

    void inject(LightningDownloadListener listener);

    void inject(PrivacySettingsFragment fragment);

    void inject(StartPage startPage);

    void inject(BrowserPresenter presenter);

    void inject(TabsManager manager);

    void inject(DebugSettingsFragment fragment);

    void inject(SuggestionsAdapter suggestionsAdapter);

}
