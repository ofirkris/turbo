package com.adblock.browser.utils;

import android.graphics.Bitmap;
import android.util.Log;

/**
 * Created by Mickael on 3/1/2017.
 */

public class ImagesUtils {
    private ImagesUtils() {}

    /**
     * Checks image size and downscales it, if one of sizes is greater than required
     * @param src - source image
     * @param maxSize - maximum size of the greater side
     * @return resized image
     */
    public static Bitmap resizeImage(Bitmap src, int maxSize) {
        int width = src.getWidth();
        int height = src.getHeight();

        if (Math.max(width, height)<=maxSize)
        {
            Log.i("MainActivity", "Bitmap has good size.");
            return src;
        }

        int newWidth, newHeight;
        if(width > height) {
            newWidth = maxSize;
            newHeight = height * maxSize / width;
        }
        else {
            newWidth = width * maxSize / height;
            newHeight = maxSize;
        }

        Bitmap retImage = Bitmap.createScaledBitmap(src, newWidth, newHeight, true);

        src.recycle();
        return retImage;
    }

}
