package com.adblock.browser.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.anthonycr.bonsai.Action;
import com.anthonycr.bonsai.Observable;
import com.anthonycr.bonsai.OnSubscribe;
import com.anthonycr.bonsai.Scheduler;
import com.anthonycr.bonsai.Subscriber;

import com.bumptech.glide.Glide;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.adblock.browser.R;
import com.adblock.browser.activity.ReadingActivity;
import com.adblock.browser.activity.TabsManager;
import com.adblock.browser.app.BrowserApp;
import com.adblock.browser.async.AsyncExecutor;
import com.adblock.browser.async.ImageDownloadTask;
import com.adblock.browser.browser.BookmarksView;
import com.adblock.browser.bus.BookmarkEvents;
import com.adblock.browser.constant.Constants;
import com.adblock.browser.controller.UIController;
import com.adblock.browser.database.BookmarkManager;
import com.adblock.browser.database.HistoryItem;
import com.adblock.browser.dialog.LightningDialogBuilder;
import com.adblock.browser.preference.PreferenceManager;
import com.adblock.browser.rest.icons.IconsAPIService;
import com.adblock.browser.rest.icons.IconsRestHelper;
import com.adblock.browser.rest.icons.model.IconResponse;
import com.adblock.browser.rest.icons.model.IconsListResponse;
import com.adblock.browser.utils.ThemeUtils;
import com.adblock.browser.view.LightningView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BookmarksFragment extends Fragment implements View.OnLongClickListener, BookmarksView {

    private final static String TAG = BookmarksFragment.class.getSimpleName();

    public final static String INCOGNITO_MODE = TAG + ".INCOGNITO_MODE";

    // Managers
    @Inject BookmarkManager mBookmarkManager;

    // Event bus
    @Inject Bus mEventBus;

    // Dialog builder
    @Inject LightningDialogBuilder mBookmarksDialogBuilder;

    @Inject PreferenceManager mPreferenceManager;

    private TabsManager mTabsManager;

    private UIController mUiController;

    // Adapter
    private BookmarkViewAdapter mBookmarkAdapter;

    // Preloaded images
    private Bitmap mWebpageBitmap, mFolderBitmap;

    // Bookmarks
    private final List<HistoryItem> mBookmarks = new ArrayList<>();

    // Views
    private ListView mBookmarksListView;
    private ImageView mBookmarkTitleImage, mBookmarkImage;

    // Colors
    private int mIconColor, mScrollIndex;

    private boolean mIsIncognito;

    private Observable<BookmarkViewAdapter> initBookmarkManager() {
        return Observable.create(new Action<BookmarkViewAdapter>() {
            @Override
            public void onSubscribe(@NonNull Subscriber<BookmarkViewAdapter> subscriber) {
                Context context = getContext();
                if (context != null) {
                    mBookmarkAdapter = new BookmarkViewAdapter(context, mBookmarks);
                    setBookmarkDataSet(mBookmarkManager.getBookmarksFromFolder(null, true), false);
                    subscriber.onNext(mBookmarkAdapter);
                }
                subscriber.onComplete();
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BrowserApp.getAppComponent().inject(this);
        final Bundle arguments = getArguments();
        final Context context = getContext();
        mUiController = (UIController) context;
        mTabsManager = mUiController.getTabModel();
        mIsIncognito = arguments.getBoolean(INCOGNITO_MODE, false);
        boolean darkTheme = mPreferenceManager.getUseTheme() != 0 || mIsIncognito;
        mWebpageBitmap = ThemeUtils.getThemedBitmap(context, R.drawable.ic_webpage, darkTheme);
        mFolderBitmap = ThemeUtils.getThemedBitmap(context, R.drawable.ic_folder, darkTheme);
        mIconColor = darkTheme ? ThemeUtils.getIconDarkThemeColor(context) :
            ThemeUtils.getIconLightThemeColor(context);
    }

    private TabsManager getTabsManager() {
        if (mTabsManager == null) {
            mTabsManager = mUiController.getTabModel();
        }
        return mTabsManager;
    }

    // Handle bookmark click
    private final OnItemClickListener mItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final HistoryItem item = mBookmarks.get(position);
            if (item.isFolder()) {
                mScrollIndex = mBookmarksListView.getFirstVisiblePosition();
                setBookmarkDataSet(mBookmarkManager.getBookmarksFromFolder(item.getTitle(), true), true);
            } else {
                mUiController.bookmarkItemClicked(item.getUrl());
            }
        }
    };

    private final OnItemLongClickListener mItemLongClickListener = new OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            final HistoryItem item = mBookmarks.get(position);
            handleLongPress(item);
            return true;
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        if (mBookmarkAdapter != null) {
            setBookmarkDataSet(mBookmarkManager.getBookmarksFromFolder(null, true), false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.bookmark_drawer, container, false);
        mBookmarksListView = (ListView) view.findViewById(R.id.right_drawer_list);
        mBookmarksListView.setOnItemClickListener(mItemClickListener);
        mBookmarksListView.setOnItemLongClickListener(mItemLongClickListener);
//        mBookmarkTitleImage = (ImageView) view.findViewById(R.id.starIcon);
//        mBookmarkTitleImage.setColorFilter(mIconColor, PorterDuff.Mode.SRC_IN);
      //  mBookmarkImage = (ImageView) view.findViewById(R.id.icon_star);
//        final View backView = view.findViewById(R.id.bookmark_back_button);
//        backView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mBookmarkManager == null) return;
//                if (!mBookmarkManager.isRootFolder()) {
//                    setBookmarkDataSet(mBookmarkManager.getBookmarksFromFolder(null, true), true);
//                    mBookmarksListView.setSelection(mScrollIndex);
//                }
//            }
//        });
//        setupNavigationButton(view, R.id.action_add_bookmark, R.id.icon_star);
//        setupNavigationButton(view, R.id.action_reading, R.id.icon_reading);
//        setupNavigationButton(view, R.id.action_toggle_desktop, R.id.icon_desktop);

        initBookmarkManager().subscribeOn(com.anthonycr.bonsai.Schedulers.io())
            .observeOn(com.anthonycr.bonsai.Schedulers.main())
            .subscribe(new OnSubscribe<BookmarkViewAdapter>() {
                @Override
                public void onNext(@Nullable BookmarkViewAdapter item) {
                    mBookmarksListView.setAdapter(mBookmarkAdapter);
                }
            });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mEventBus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mEventBus.unregister(this);
    }

    public void reinitializePreferences() {
        Activity activity = getActivity();
        if (activity == null) {
            return;
        }
        boolean darkTheme = mPreferenceManager.getUseTheme() != 0 || mIsIncognito;
        mWebpageBitmap = ThemeUtils.getThemedBitmap(activity, R.drawable.ic_webpage, darkTheme);
        mFolderBitmap = ThemeUtils.getThemedBitmap(activity, R.drawable.ic_folder, darkTheme);
        mIconColor = darkTheme ? ThemeUtils.getIconDarkThemeColor(activity) :
            ThemeUtils.getIconLightThemeColor(activity);
    }

    private void updateBookmarkIndicator(final String url) {
        if (!mBookmarkManager.isBookmark(url)) {
            mBookmarkImage.setImageResource(R.drawable.ic_action_star);
            mBookmarkImage.setColorFilter(mIconColor, PorterDuff.Mode.SRC_IN);
        } else {
            mBookmarkImage.setImageResource(R.drawable.ic_bookmark);
            mBookmarkImage.setColorFilter(ThemeUtils.getAccentColor(getContext()), PorterDuff.Mode.SRC_IN);
        }
    }

    @Subscribe
    public void bookmarkDeleted(@NonNull final BookmarkEvents.Deleted event) {
        mBookmarks.remove(event.item);
        if (event.item.isFolder()) {
            setBookmarkDataSet(mBookmarkManager.getBookmarksFromFolder(null, true), false);
        } else {
            mBookmarkAdapter.notifyDataSetChanged();
        }
    }

    private void setBookmarkDataSet(@NonNull List<HistoryItem> items, boolean animate) {

        mBookmarks.clear();
        mBookmarks.addAll(items);
        mBookmarkAdapter.notifyDataSetChanged();
//        final int resource;
//        if (mBookmarkManager.isRootFolder()) {
//            resource = R.drawable.ic_action_star;
//        } else {
//            resource = R.drawable.ic_action_back;
//        }
//
//        final Animation startRotation = new Animation() {
//            @Override
//            protected void applyTransformation(float interpolatedTime, Transformation t) {
//                mBookmarkTitleImage.setRotationY(90 * interpolatedTime);
//            }
//        };
//        final Animation finishRotation = new Animation() {
//            @Override
//            protected void applyTransformation(float interpolatedTime, Transformation t) {
//                mBookmarkTitleImage.setRotationY((-90) + (90 * interpolatedTime));
//            }
//        };
//        startRotation.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                mBookmarkTitleImage.setImageResource(resource);
//                mBookmarkTitleImage.startAnimation(finishRotation);
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//            }
//        });
//        startRotation.setInterpolator(new AccelerateInterpolator());
//        finishRotation.setInterpolator(new DecelerateInterpolator());
//        startRotation.setDuration(250);
//        finishRotation.setDuration(250);
//
//        if (animate) {
//            mBookmarkTitleImage.startAnimation(startRotation);
//        } else {
//            mBookmarkTitleImage.setImageResource(resource);
//        }
    }

    private void setupNavigationButton(@NonNull View view, @IdRes int buttonId, @IdRes int imageId) {
        FrameLayout frameButton = (FrameLayout) view.findViewById(buttonId);
       // frameButton.setOnClickListener(this);
        frameButton.setOnLongClickListener(this);
        ImageView buttonImage = (ImageView) view.findViewById(imageId);
        buttonImage.setColorFilter(mIconColor, PorterDuff.Mode.SRC_IN);
    }

    private void handleLongPress(@NonNull final HistoryItem item) {
        if (item.isFolder()) {
            mBookmarksDialogBuilder.showBookmarkFolderLongPressedDialog(getActivity(), item);
        } else {
            mBookmarksDialogBuilder.showLongPressedDialogForBookmarkUrl(getActivity(), item);
        }
    }

//    @Override
//    public void onClick(@NonNull View v) {
//        switch (v.getId()) {
//            case R.id.action_add_bookmark:
//                mUiController.bookmarkButtonClicked();
//                break;
//            case R.id.action_reading:
//                LightningView currentTab = getTabsManager().getCurrentTab();
//                if (currentTab != null) {
//                    Intent read = new Intent(getActivity(), ReadingActivity.class);
//                    read.putExtra(Constants.LOAD_READING_URL, currentTab.getUrl());
//                    startActivity(read);
//                }
//                break;
//            case R.id.action_toggle_desktop:
//                LightningView current = getTabsManager().getCurrentTab();
//                if (current != null) {
//                    current.toggleDesktopUA(getActivity());
//                    current.reload();
//                    // TODO add back drawer closing
//                }
//                break;
//            default:
//                break;
//        }
//    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public void navigateBack() {
        if (mBookmarkManager.isRootFolder()) {
            //mUiController.closeBookmarksDrawer();
        } else {
            setBookmarkDataSet(mBookmarkManager.getBookmarksFromFolder(null, true), true);
            mBookmarksListView.setSelection(mScrollIndex);
        }
    }

    @Override
    public void handleUpdatedUrl(@NonNull String url) {
        updateBookmarkIndicator(url);
        String folder = mBookmarkManager.getCurrentFolder();
        setBookmarkDataSet(mBookmarkManager.getBookmarksFromFolder(folder, true), false);
    }

    private class BookmarkViewAdapter extends ArrayAdapter<HistoryItem> {

        final Context context;

        public BookmarkViewAdapter(Context context, @NonNull List<HistoryItem> data) {
            super(context, R.layout.bookmark_list_item, data);
            this.context = context;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            View row = convertView;
            final BookmarkViewHolder holder;

            if (row == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                row = inflater.inflate(R.layout.bookmark_list_item, parent, false);

                holder = new BookmarkViewHolder();
                holder.txtTitle = (TextView) row.findViewById(R.id.textBookmark);
                holder.favicon = (ImageView) row.findViewById(R.id.faviconBookmark);
                row.setTag(holder);
            } else {
                holder = (BookmarkViewHolder) row.getTag();
            }

            ViewCompat.jumpDrawablesToCurrentState(row);

            final HistoryItem web = mBookmarks.get(position);
            holder.txtTitle.setText(web.getTitle());
            setImageWebIcon(web, holder.favicon);
            return row;
        }

        private void setImageWebIcon(final HistoryItem web, final ImageView ivWebIcon)
        {
            if (web.isFolder()) {
                ivWebIcon.setImageBitmap(mFolderBitmap);
            }else
            {
                IconsAPIService apiService = IconsRestHelper.getIconsAPIService();
                rx.Observable<IconsListResponse> iconsObservable= apiService.getIconsFromSite(web.getUrl());
                iconsObservable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new rx.Subscriber<IconsListResponse>() {

                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(@NonNull Throwable throwable) {
                                setImageFavIcon(web, ivWebIcon);
                            }

                            @Override
                            public void onNext(@Nullable IconsListResponse item) {
                                if(item == null || item.getIconsList() == null || item.getIconsList().size() == 0 || item.getIconsList().get(0) == null || TextUtils.isEmpty(item.getIconsList().get(0).getUrl()))
                                {
                                    setImageFavIcon(web, ivWebIcon);
                                    return;
                                }
                                Glide.with(getContext())
                                        .load(item.getIconsList().get(0).getUrl())
                                        .into(ivWebIcon);
                            }

                        });
            }
        }

        private void setImageFavIcon(HistoryItem web, ImageView ivFavIcon) {
            if (web.getBitmap() == null) {
                ivFavIcon.setImageBitmap(mWebpageBitmap);
                new ImageDownloadTask(ivFavIcon, web, mWebpageBitmap, BrowserApp.get(context))
                    .executeOnExecutor(AsyncExecutor.getInstance());
            } else {
                ivFavIcon.setImageBitmap(web.getBitmap());
            }
        }

        private class BookmarkViewHolder {
            TextView txtTitle;
            ImageView favicon;
        }
    }

}
