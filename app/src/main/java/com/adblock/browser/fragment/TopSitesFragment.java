package com.adblock.browser.fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.adblock.browser.model.SettingsItem;
import com.adblock.browser.R;
import com.adblock.browser.activity.TabsManager;
import com.adblock.browser.app.BrowserApp;
import com.adblock.browser.controller.UIController;
import com.adblock.browser.database.HistoryItem;
import com.adblock.browser.model.TopSite;
import com.adblock.browser.preference.PreferenceManager;
import com.adblock.browser.utils.ThemeUtils;
import com.adblock.browser.view.SettingsItemsManager;
import com.adblock.browser.view.SpacesItemDecoration;

public class TopSitesFragment extends Fragment {

    private final static String TAG = TopSitesFragment.class.getSimpleName();

    public final static String INCOGNITO_MODE = TAG + ".INCOGNITO_MODE";
    private boolean darkTheme;

    @Inject PreferenceManager mPreferenceManager;

    private UIController mUiController;

    private boolean mIsIncognito;

    private List<TopSite> topSites;
    private Context mContext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BrowserApp.getAppComponent().inject(this);
        final Bundle arguments = getArguments();
        mContext = getContext();
        mUiController = (UIController) mContext;

       // darkTheme = arguments.getBoolean(INCOGNITO_MODE, false);

        setupTopSitesList();

    }

    private void setupTopSitesList()
    {
        topSites = new ArrayList<>();

        InputStream inputStream = mContext.getResources().openRawResource(R.raw.top_sites);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        topSites = new Gson().fromJson(reader, new TypeToken<List<TopSite>>(){}.getType());
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.top_sites_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView rvTopSites = (RecyclerView) view.findViewById(R.id.rv_top_sites);

        TopSitesAdapter topSitesAdapter = new TopSitesAdapter(mContext,topSites);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext,2);

        rvTopSites.addItemDecoration(new SpacesItemDecoration(20));

        rvTopSites.setLayoutManager(gridLayoutManager);

        rvTopSites.setAdapter(topSitesAdapter);

    }

    public class TopSitesAdapter extends RecyclerView.Adapter<TopSitesAdapter.TopSiteViewHolder> {

        public class TopSiteViewHolder extends RecyclerView.ViewHolder {
            private TextView tvTopSite;
            private ImageView ivTopSite;
            private FrameLayout flTopSite;
            public TopSiteViewHolder(View itemView) {
                super(itemView);
                tvTopSite = (TextView) itemView.findViewById(R.id.tvTopSite);
                ivTopSite = (ImageView) itemView.findViewById(R.id.ivTopSite);
                flTopSite = (FrameLayout) itemView.findViewById(R.id.flTopSite);
            }
        }

        private Context context;
        private List<TopSite> topSites;
        public TopSitesAdapter(Context context, List<TopSite> topSites) {
            this.topSites = topSites;
            this.context = context;
        }

        @Override
        public TopSiteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new TopSiteViewHolder(LayoutInflater.from(context)
                    .inflate(R.layout.top_sites_item, parent, false));
        }

        @Override
        public void onBindViewHolder(final TopSiteViewHolder holder, final int position) {
            final TopSite topSite = topSites.get(position);
            int logoResId = context.getResources().getIdentifier(topSite.getDrawableName(), "drawable", context.getPackageName());
            holder.ivTopSite.setImageResource(logoResId);
            holder.flTopSite.setBackgroundColor(Color.parseColor(topSite.getBgColor()));

            holder.tvTopSite.setText(topSite.getSiteName());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mUiController.bookmarkItemClicked(topSite.getSiteUrl());
                }
            });



        }

        @Override
        public int getItemCount() {
            return topSites == null ? 0 : topSites.size();
        }

    }


}
