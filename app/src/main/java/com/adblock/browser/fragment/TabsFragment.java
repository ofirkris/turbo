package com.adblock.browser.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import com.adblock.browser.R;
import com.adblock.browser.activity.TabsManager;
import com.adblock.browser.app.BrowserApp;
import com.adblock.browser.browser.TabsView;
import com.adblock.browser.controller.UIController;
import com.adblock.browser.fragment.anim.HorizontalItemAnimator;
import com.adblock.browser.fragment.anim.VerticalItemAnimator;
import com.adblock.browser.preference.PreferenceManager;
import com.adblock.browser.rest.icons.IconsAPIService;
import com.adblock.browser.rest.icons.IconsRestHelper;
import com.adblock.browser.rest.icons.model.IconsListResponse;
import com.adblock.browser.utils.DrawableUtils;
import com.adblock.browser.utils.ThemeUtils;
import com.adblock.browser.utils.Utils;
import com.adblock.browser.view.BackgroundDrawable;
import com.adblock.browser.view.LightningView;
import com.adblock.browser.view.TextIconItem;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A fragment that holds and manages the tabs and interaction with the tabs.
 * It is reliant on the BrowserController in order to get the current UI state
 * of the browser. It also uses the BrowserController to signal that the UI needs
 * to change. This class contains the adapter used by both the drawer tabs and
 * the desktop tabs. It delegates touch events for the tab UI appropriately.
 */
public class TabsFragment extends Fragment implements View.OnClickListener, View.OnLongClickListener, TabsView {

    private static final String TAG = TabsFragment.class.getSimpleName();

    /**
     * Arguments boolean to tell the fragment it is displayed in the drawner or on the tab strip
     * If true, the fragment is in the left drawner in the strip otherwise.
     */
    public static final String VERTICAL_MODE = TAG + ".VERTICAL_MODE";
    public static final String IS_INCOGNITO = TAG + ".IS_INCOGNITO";

    private boolean mIsIncognito, mDarkTheme;
    private int mIconColor;
    private boolean mColorMode = true;


    @Nullable private LightningViewAdapter mTabsAdapter;
    private UIController mUiController;
    private RecyclerView mRecyclerView;

    private TabsManager mTabsManager;
    @Inject Bus mBus;
    @Inject PreferenceManager mPreferences;

    public TabsFragment() {
        BrowserApp.getAppComponent().inject(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle arguments = getArguments();
        final Context context = getContext();
        mUiController = (UIController) getActivity();
        mTabsManager = mUiController.getTabModel();
        mIsIncognito = arguments.getBoolean(IS_INCOGNITO, false);

        mDarkTheme = mPreferences.getUseTheme() != 0 || mIsIncognito;
        mColorMode = mPreferences.getColorModeEnabled();
        mColorMode &= !mDarkTheme;
        mIconColor = mDarkTheme ?
            ThemeUtils.getIconDarkThemeColor(context) :
            ThemeUtils.getIconLightThemeColor(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view;
        final LayoutManager layoutManager;

        view = inflater.inflate(R.layout.tab_drawer, container, false);
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        //setupFrameLayoutButton(view, R.id.tab_header_button, R.id.plusIcon);
//        setupFrameLayoutButton(view, R.id.new_tab_button, R.id.icon_plus);
//        setupFrameLayoutButton(view, R.id.action_back, R.id.icon_back);
//        setupFrameLayoutButton(view, R.id.action_forward, R.id.icon_forward);
//        setupFrameLayoutButton(view, R.id.action_home, R.id.icon_home);

        view.findViewById(R.id.tiiCloseAllTabs).setOnClickListener(this);
        view.findViewById(R.id.tiiIncognito).setOnClickListener(this);
        view.findViewById(R.id.tiiNewTab).setOnClickListener(this);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.tabs_list);
        SimpleItemAnimator animator;
        animator = new VerticalItemAnimator();

        animator.setSupportsChangeAnimations(false);
        animator.setAddDuration(200);
        animator.setChangeDuration(0);
        animator.setRemoveDuration(200);
        animator.setMoveDuration(200);
        mRecyclerView.setLayerType(View.LAYER_TYPE_NONE, null);
        mRecyclerView.setItemAnimator(animator);
        mRecyclerView.setLayoutManager(layoutManager);
        mTabsAdapter = new LightningViewAdapter();
        mRecyclerView.setAdapter(mTabsAdapter);
        mRecyclerView.setHasFixedSize(true);
        return view;
    }

    private TabsManager getTabsManager() {
        if (mTabsManager == null) {
            mTabsManager = mUiController.getTabModel();
        }
        return mTabsManager;
    }

    private void setupFrameLayoutButton(@NonNull final View root, @IdRes final int buttonId,
                                        @IdRes final int imageId) {
        final View frameButton = root.findViewById(buttonId);
        final ImageView buttonImage = (ImageView) root.findViewById(imageId);
        frameButton.setOnClickListener(this);
        frameButton.setOnLongClickListener(this);
        buttonImage.setColorFilter(mIconColor, PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mTabsAdapter = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        mBus.register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        // Force adapter refresh
//        if (mTabsAdapter != null) {
//            mTabsAdapter.notifyDataSetChanged();
//        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mBus.unregister(this);
    }

    @Override
    public void tabsInitialized() {
        if (mTabsAdapter != null) {
            mTabsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void refreshTabs(int position) {
        if (mTabsAdapter != null) {
            mTabsAdapter.notifyItemChanged(position);
        }
    }

    public void reinitializePreferences() {
        Activity activity = getActivity();
        if (activity == null) {
            return;
        }
        mDarkTheme = mPreferences.getUseTheme() != 0 || mIsIncognito;
        mColorMode = mPreferences.getColorModeEnabled();
        mColorMode &= !mDarkTheme;
        mIconColor = mDarkTheme ?
            ThemeUtils.getIconDarkThemeColor(activity) :
            ThemeUtils.getIconLightThemeColor(activity);
        if (mTabsAdapter != null) {
            mTabsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(@NonNull View v) {
        switch (v.getId()) {
//            case R.id.tab_header_button:
//                mUiController.showCloseDialog(getTabsManager().indexOfCurrentTab());
//                break;
            case R.id.tiiNewTab:
                mUiController.newTabButtonClicked();
                break;
            case R.id.tiiCloseAllTabs:
                mUiController.closeBrowser(true);
                break;
            case R.id.tiiIncognito:
                mUiController.newIncognitoButtonClicked();
                break;
//            case R.id.action_home:
//                mUiController.onHomeButtonPressed();
            default:
                break;

        }
        mUiController.closeTabsDrawer();
    }

    @Override
    public boolean onLongClick(@NonNull View v) {
        switch (v.getId()) {
            case R.id.action_new_tab:
                mUiController.newTabButtonLongClicked();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void tabAdded() {
        if (mTabsAdapter != null) {
            mTabsAdapter.notifyItemInserted(getTabsManager().last());
            mRecyclerView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mRecyclerView.smoothScrollToPosition(mTabsAdapter.getItemCount() - 1);
                }
            }, 500);
        }
    }

    @Override
    public void tabRemoved(int position) {
        if (mTabsAdapter != null) {
            mTabsAdapter.notifyItemRemoved(position);
        }
    }

    @Override
    public void tabChanged(int position) {
        if (mTabsAdapter != null) {
         //   mTabsAdapter.notifyItemChanged(position);
        }
    }

    private class LightningViewAdapter extends RecyclerView.Adapter<LightningViewAdapter.LightningViewHolder> {

        private final int mLayoutResourceId;
        private ColorMatrix mColorMatrix;
        private Paint mPaint;
        private ColorFilter mFilter;
        private static final float DESATURATED = 0.5f;


        public LightningViewAdapter() {
            this.mLayoutResourceId = R.layout.tab_list_item;
        }

        @NonNull
        @Override
        public LightningViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
            View view = inflater.inflate(mLayoutResourceId, viewGroup, false);
            DrawableUtils.setBackground(view, new BackgroundDrawable(view.getContext()));

            return new LightningViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final LightningViewHolder holder, int position) {
            holder.exitButton.setTag(position);

            ViewCompat.jumpDrawablesToCurrentState(holder.exitButton);

            final LightningView web = getTabsManager().getTabAtPosition(position);
            if (web == null) {
                return;
            }
            holder.txtTitle.setText(web.getTitle());

           // final Bitmap favicon = web.getFavicon();
            if (web.isForegroundTab()) {
                Drawable foregroundDrawable = null;
//                if (!mIsIncognito && mColorMode) {
//                    mUiController.changeToolbarBackground(favicon, foregroundDrawable);
//                }

                TextViewCompat.setTextAppearance(holder.txtTitle, R.style.boldText);
               // holder.favicon.setImageBitmap(favicon);
            } else {
                TextViewCompat.setTextAppearance(holder.txtTitle, R.style.normalText);
               // holder.favicon.setImageBitmap(getDesaturatedBitmap(favicon));
            }

            if(!web.isHomeScreenShown())
            {
                web.getWebViewScreenshot(new LightningView.WebviewScreenshotsCallbacks() {
                    @Override
                    public void onScreenshotReady(Bitmap screenshot) {
                        if(screenshot != null)
                            holder.ivWebPreview.setImageBitmap(screenshot);
                        else {
                            getWebsiteIcon(web,holder.ivWebPreview);
                        }
                    }
                });
            }


            BackgroundDrawable verticalBackground = (BackgroundDrawable) holder.layout.getBackground();
            verticalBackground.setCrossFadeEnabled(false);
            if (web.isForegroundTab()) {
                verticalBackground.startTransition(200);
            } else {
                verticalBackground.reverseTransition(200);
            }

        }

        private void getWebsiteIcon(final LightningView web, final ImageView ivWebIcon)
        {
            IconsAPIService apiService = IconsRestHelper.getIconsAPIService();
            rx.Observable<IconsListResponse> iconsObservable= apiService.getIconsFromSite(web.getUrl());
            iconsObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new rx.Subscriber<IconsListResponse>() {

                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(@NonNull Throwable throwable) {
                            ivWebIcon.setImageBitmap(web.getFavicon());
                        }

                        @Override
                        public void onNext(@Nullable IconsListResponse item) {
                            if(item == null || item.getIconsList() == null || item.getIconsList().size() == 0 || item.getIconsList().get(0) == null || TextUtils.isEmpty(item.getIconsList().get(0).getUrl()))
                            {
                                ivWebIcon.setImageBitmap(web.getFavicon());
                                return;
                            }
                            Glide.with(getContext())
                                    .load(item.getIconsList().get(0).getUrl())
                                    .into(ivWebIcon);
                        }

                    });
        }

        @Override
        public int getItemCount() {
            return getTabsManager().size();
        }

        public Bitmap getDesaturatedBitmap(@NonNull Bitmap favicon) {
            Bitmap grayscaleBitmap = Bitmap.createBitmap(favicon.getWidth(),
                favicon.getHeight(), Bitmap.Config.ARGB_8888);

            Canvas c = new Canvas(grayscaleBitmap);
            if (mColorMatrix == null || mFilter == null || mPaint == null) {
                mPaint = new Paint();
                mColorMatrix = new ColorMatrix();
                mColorMatrix.setSaturation(DESATURATED);
                mFilter = new ColorMatrixColorFilter(mColorMatrix);
                mPaint.setColorFilter(mFilter);
            }

            c.drawBitmap(favicon, 0, 0, mPaint);
            return grayscaleBitmap;
        }

        public class LightningViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

            public LightningViewHolder(@NonNull View view) {
                super(view);
                txtTitle = (TextView) view.findViewById(R.id.textTab);
                ivWebPreview = (ImageView) view.findViewById(R.id.iv_web_preview);
                exit = (ImageView) view.findViewById(R.id.deleteButton);
                layout = (RelativeLayout) view.findViewById(R.id.tab_item_background);
                exitButton = (FrameLayout) view.findViewById(R.id.deleteAction);
                //exit.setColorFilter(mIconColor, PorterDuff.Mode.SRC_IN);

                exitButton.setOnClickListener(this);
                layout.setOnClickListener(this);
                layout.setOnLongClickListener(this);
            }

            @NonNull final TextView txtTitle;
            @NonNull final ImageView ivWebPreview;
            @NonNull final ImageView exit;
            @NonNull final FrameLayout exitButton;
            @NonNull final RelativeLayout layout;

            @Override
            public void onClick(View v) {
                if (v == exitButton) {
                    mUiController.tabCloseClicked(getAdapterPosition());
                }
                if (v == layout) {
                    mUiController.tabClicked(getAdapterPosition());
                }
            }

            @Override
            public boolean onLongClick(View v) {
                mUiController.showCloseDialog(getAdapterPosition());
                return true;
            }
        }
    }
}
