package com.adblock.browser.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.adblock.browser.model.SettingsItem;
import com.adblock.browser.R;
import com.adblock.browser.activity.TabsManager;
import com.adblock.browser.app.BrowserApp;
import com.adblock.browser.controller.UIController;
import com.adblock.browser.database.HistoryItem;
import com.adblock.browser.preference.PreferenceManager;
import com.adblock.browser.utils.ThemeUtils;
import com.adblock.browser.view.SettingsItemsManager;

public class SettingsFragment extends Fragment implements SettingsItemsManager.SettingsItemsCallbacks{

    private final static String TAG = SettingsFragment.class.getSimpleName();

    public final static String INCOGNITO_MODE = TAG + ".INCOGNITO_MODE";
    private boolean darkTheme;

    private SettingsItemsManager settingsItemsManager;

    @Inject PreferenceManager mPreferenceManager;

    private UIController mUiController;

    private boolean mIsIncognito;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BrowserApp.getAppComponent().inject(this);
        final Bundle arguments = getArguments();
        final Context context = getContext();
        mUiController = (UIController) context;

        darkTheme = arguments.getBoolean(INCOGNITO_MODE, false);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.settings_drawer, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        settingsItemsManager = new SettingsItemsManager(
                getContext(),
                darkTheme,
                (RecyclerView) view.findViewById(R.id.right_drawer_list),
                (RelativeLayout) view.findViewById(R.id.settings_top_bar),
                this

        );
        settingsItemsManager.init();

    }

    @Override
    public void onItemClicked(SettingsItem item) {
        mUiController.onSettingsItemClicked(item);
    }

    @Override
    public void onItemChecked(boolean isChecked, SettingsItem item) {
        mUiController.onSettingsItemChecked(item, isChecked);
    }

    @Override
    public void onTopBarItemClicked(SettingsItemsManager.SETTINGS_ITEM_BAR_TYPE settingsItemBarType) {
        mUiController.onSettingsTopBarItemClicked(settingsItemBarType);
    }

    public void setForwardArrowEnabled(boolean enabled)
    {
        settingsItemsManager.setForwardButtonEnabled(enabled);
    }
    public void setBackwardsArrowEnabled(boolean enabled)
    {
        settingsItemsManager.setBackwardsButtonEnabled(enabled);
    }
    public void setBookmarkButtonEnabled(boolean enabled)
    {
        settingsItemsManager.setBookmarkButtonEnabled(enabled);
    }

}
