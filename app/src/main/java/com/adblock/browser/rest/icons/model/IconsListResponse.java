package com.adblock.browser.rest.icons.model;

import java.util.List;

/**
 * Created by Mickael on 3/6/2017.
 */
public class IconsListResponse {

    private String url;
    private List<IconResponse> icons;

    public String getUrl() {
        return url;
    }

    public List<IconResponse> getIconsList() {
        return icons;
    }
}
