package com.adblock.browser.rest.icons;


import java.util.List;

import com.adblock.browser.rest.icons.model.IconResponse;
import com.adblock.browser.rest.icons.model.IconsListResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Mickael on 21/11/2016.
 */

public interface IconsAPIService {

    @GET("allicons.json")
    Observable<IconsListResponse> getIconsFromSite(@Query("url") String url);


}
