package com.adblock.browser.rest.icons;


import com.adblock.browser.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mickael on 21/11/2016.
 */
public class IconsRestHelper {
    public static final String ICONS_BASE_URL = "https://icons.better-idea.org/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .baseUrl(ICONS_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    public static IconsAPIService getIconsAPIService() {
        if(BuildConfig.DEBUG)
        {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);
        }
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(IconsAPIService.class);
    }

}
