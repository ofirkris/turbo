package com.adblock.browser.rest.icons.model;

/**
 * Created by Mickael on 3/6/2017.
 */

public class IconResponse {
    private String url;
    private int width;
    private int height;

    public String getUrl() {
        return url;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
