package com.adblock.browser.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.adblock.browser.model.SettingsItem;
import com.adblock.browser.R;
import com.adblock.browser.app.BrowserApp;
import com.adblock.browser.preference.PreferenceManager;
import com.adblock.browser.utils.ThemeUtils;

/**
 * Created by Mickael on 3/6/2017.
 */
public class SettingsItemsManager {

    public enum SETTINGS_ITEM_BAR_TYPE {
        BUTTON_FORWARD,
        BUTTON_BACKWARDS,
        BUTTON_BOOKMARK,
        BUTTON_REFRESH,
    }

    public interface SettingsItemsCallbacks {
        void onItemClicked(SettingsItem item);
        void onItemChecked(boolean isChecked, SettingsItem item);
        void onTopBarItemClicked(SETTINGS_ITEM_BAR_TYPE settings_item_bar_type);
    }

    @Inject
    PreferenceManager mPreferenceManager;

    private ImageView ivArrowForward;
    private ImageView ivArrowBackwards;
    private ImageView ivBookmark;
    private ImageView ivRefresh;

    private List<SettingsItem> settingsItems;
    private Context mContext;
    private boolean darkTheme;
    private RecyclerView mRecyclerView;
    private SettingsItemsCallbacks settingsItemsCallbacks;
    private RelativeLayout prlContainer;

    private boolean isBookmark = false;

    // Colors
    private int mIconColor, mDisabledIconColor;

    public SettingsItemsManager(Context mContext, boolean darkTheme, RecyclerView mRecyclerView, RelativeLayout prlContainer, SettingsItemsCallbacks settingsItemsCallbacks) {
        BrowserApp.getAppComponent().inject(this);
        this.mContext = mContext;
        this.darkTheme = darkTheme;
        this.mRecyclerView = mRecyclerView;
        this.settingsItemsCallbacks = settingsItemsCallbacks;
        this.prlContainer = prlContainer;

        mIconColor = ThemeUtils.getIconLightThemeColor(mContext);
        mDisabledIconColor = ContextCompat.getColor(mContext, R.color.icon_light_theme_disabled);

    }

    public void init()
    {
        setupDrawerItems();

        SettingsItemsAdapter settingsItemsAdapter = new SettingsItemsAdapter(mContext,settingsItems, settingsItemsCallbacks);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(settingsItemsAdapter);

        setupTopBar();

    }

    public void setForwardButtonEnabled(boolean enabled)
    {
        setArrowEnabled(ivArrowForward,enabled);
    }

    public void setBackwardsButtonEnabled(boolean enabled)
    {
        setArrowEnabled(ivArrowBackwards,enabled);
    }

    private void setArrowEnabled(ImageView ivArrow,boolean enabled)
    {
        int color = enabled ? mIconColor : mDisabledIconColor;
        ivArrow.getDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        ivArrow.setEnabled(enabled);
    }

    public void setBookmarkButtonEnabled(boolean enabled)
    {
        isBookmark = enabled;

        if (!enabled) {
            ivBookmark.setImageResource(R.drawable.settings_bookmark);
            ivBookmark.setColorFilter(mIconColor, PorterDuff.Mode.SRC_IN);
        } else {
            ivBookmark.setImageResource(R.drawable.settings_bookmark_full);
            ivBookmark.setColorFilter(ThemeUtils.getAccentColor(mContext), PorterDuff.Mode.SRC_IN);
        }
    }

    private void setupTopBar()
    {
        ivArrowForward = (ImageView) prlContainer.findViewById(R.id.iv_forward_arrow);
        ivArrowBackwards = (ImageView) prlContainer.findViewById(R.id.iv_back_arrow);
        ivBookmark = (ImageView) prlContainer.findViewById(R.id.iv_settings_bookmark);
        ivRefresh = (ImageView) prlContainer.findViewById(R.id.iv_settings_refresh);

        ivArrowForward.setOnClickListener(topBarClickListener);
        ivArrowBackwards.setOnClickListener(topBarClickListener);
        ivBookmark.setOnClickListener(topBarClickListener);
        ivRefresh.setOnClickListener(topBarClickListener);
    }

    private View.OnClickListener topBarClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(settingsItemsCallbacks != null)
                settingsItemsCallbacks.onTopBarItemClicked(getItemBarType(v.getId()));
        }
    };

    private SETTINGS_ITEM_BAR_TYPE getItemBarType(int viewId)
    {
        switch (viewId)
        {
            case R.id.iv_forward_arrow:
                return SETTINGS_ITEM_BAR_TYPE.BUTTON_FORWARD;
            case R.id.iv_back_arrow:
                return SETTINGS_ITEM_BAR_TYPE.BUTTON_BACKWARDS;
            case R.id.iv_settings_bookmark:
                isBookmark = !isBookmark;
                setBookmarkButtonEnabled(isBookmark);
                return SETTINGS_ITEM_BAR_TYPE.BUTTON_BOOKMARK;
            case R.id.iv_settings_refresh:
                return SETTINGS_ITEM_BAR_TYPE.BUTTON_REFRESH;
        }
        return null;
    }

    private void setupDrawerItems()
    {
        settingsItems = new ArrayList<>();

        TypedArray settingItemsIcons  = mContext.getResources().obtainTypedArray(R.array.settings_drawer_icons);
        TypedArray settingsItemsLabels = mContext.getResources().obtainTypedArray(R.array.settings_drawer_labels);

        //Push to list
        for (int i = 0; i < settingItemsIcons.length(); i++)
        {
            int itemStringResId = settingsItemsLabels.getResourceId(i, 0);
            SettingsItem settingsItem = new SettingsItem(
                    mContext.getString(itemStringResId),
                    ThemeUtils.getThemedBitmap(mContext,settingItemsIcons.getResourceId(i, 0), darkTheme),
                    itemStringResId == R.string.block_ads || itemStringResId == R.string.action_desktop_site,
                    SettingsItem.inferItemType(itemStringResId)
            );

            if(settingsItem.isCheckable())
            {
                settingsItem.setChecked(
                        (settingsItemsLabels.getResourceId(i, 0) == R.string.block_ads && mPreferenceManager.getAdBlockEnabled()) ||
                                (settingsItemsLabels.getResourceId(i, 0) == R.string.action_desktop_site && mPreferenceManager.getUserAgentChoice() == 2)
                );
            }

            settingsItems.add(settingsItem);
        }

        settingsItemsLabels.recycle();
        settingItemsIcons.recycle();
    }

    public class SettingsItemsAdapter extends RecyclerView.Adapter<SettingsItemsAdapter.SettingsItemViewHolder> {

        public class SettingsItemViewHolder extends RecyclerView.ViewHolder {
            private TextView tvSettingsLabel;
            private ImageView ivSettingsIcon;
            private SwitchButton cbSettings;
            public SettingsItemViewHolder(View itemView) {
                super(itemView);
                tvSettingsLabel = (TextView) itemView.findViewById(R.id.tv_settings_label);
                ivSettingsIcon = (ImageView) itemView.findViewById(R.id.iv_settings_icon);
                cbSettings = (SwitchButton) itemView.findViewById(R.id.cb_settings_item);
            }
        }

        private Context context;
        private List<SettingsItem> settingsItems;
        private SettingsItemsCallbacks settingsItemsCallbacks;
        public SettingsItemsAdapter(Context context, List<SettingsItem> settingsItems, SettingsItemsCallbacks settingsItemsCallbacks) {
            this.settingsItems = settingsItems;
            this.context = context;
            this.settingsItemsCallbacks = settingsItemsCallbacks;
        }

        @Override
        public SettingsItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new SettingsItemViewHolder(LayoutInflater.from(context)
                    .inflate(R.layout.settings_list_item, parent, false));
        }

        @Override
        public void onBindViewHolder(final SettingsItemViewHolder holder, final int position) {
            final SettingsItem settingsItem = settingsItems.get(position);

            holder.ivSettingsIcon.setImageBitmap(settingsItem.getItemIcon());
            holder.tvSettingsLabel.setText(settingsItem.getItemName());

            if(settingsItem.isCheckable())
            {
                holder.cbSettings.setChecked(settingsItem.isChecked());

                holder.cbSettings.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                        if(settingsItemsCallbacks != null)
                            settingsItemsCallbacks.onItemChecked(isChecked, settingsItem);
                    }
                });

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.cbSettings.toggle();
                    }
                });

            }
            else
            {
                holder.cbSettings.setVisibility(View.INVISIBLE);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(settingsItemsCallbacks != null)
                            settingsItemsCallbacks.onItemClicked(settingsItem);
                    }
                });
            }

        }

        @Override
        public int getItemCount() {
            return settingsItems == null ? 0 : settingsItems.size();
        }


    }


}
