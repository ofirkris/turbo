package com.adblock.browser.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adblock.browser.R;


/**
 * Created by Mickael on 12/1/2016.
 */
public class TextIconItem extends RelativeLayout {
    private TextView tvItem;
    private ImageView ivItem;
    private View vDivider;
    public TextIconItem(Context context) {
        super(context);
        init(null);
    }

    public TextIconItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public TextIconItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(@Nullable AttributeSet attrs) {
        inflate(getContext(), R.layout.text_icon_item, this);
        this.tvItem = (TextView)findViewById(R.id.tv_item);
        this.ivItem = (ImageView)findViewById(R.id.iv_item);
        this.vDivider = findViewById(R.id.item_divider);
        if(attrs != null)
        {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs,
                    R.styleable.TextIconItemAttrs);

            int count = typedArray.getIndexCount();
            try{
                for (int i = 0; i < count; ++i) {

                    int attr = typedArray.getIndex(i);
                    // the attr corresponds to the title attribute
                    if(attr == R.styleable.TextIconItemAttrs_settings_item_text) {
                        String text = typedArray.getString(attr);
                        if(text != null)
                            setText(text);
                    } else if(attr == R.styleable.TextIconItemAttrs_settings_item_icon) {
                        int resId = typedArray.getResourceId(attr, R.drawable.ic_incognito);
                        setResId(resId);
                    }
                    else if(attr == R.styleable.TextIconItemAttrs_settings_item_show_divider) {
                        boolean showDivider = typedArray.getBoolean(attr, true);
                        setDividerVisibility(showDivider);
                    }
                }
            }
            // the recycle() will be executed obligatorily
            finally {
                // for reuse
                typedArray.recycle();
            }
        }
    }

    public void setText(String txt)
    {
        tvItem.setText(txt);
    }

    public String getText()
    {
        return tvItem.getText().toString();
    }

    public void setResId(int iconRes)
    {
        ivItem.setImageResource(iconRes);
    }

    public void setDividerVisibility(boolean isVisible)
    {
        vDivider.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }


}
