package com.adblock.browser.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.adblock.browser.R;
import com.adblock.browser.fragment.BookmarksFragment;
import com.adblock.browser.fragment.TopSitesFragment;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Mickael on 3/5/2017.
 */

public class HomePageView extends RelativeLayout{
    public static final int HOMEPAGE_TOP_SITES = 0;
    public static final int HOMEPAGE_BOOKMARKS = 1;
    private Context context;
    LayoutInflater mInflater;
    private boolean showBookmarks;
    @Bind(R.id.tl_homepage) TabLayout tlHomepage;
    @Bind(R.id.vp_homepage) ViewPager vpHomepage;

    public HomePageView(Context context, boolean showBookmarks) {
        super(context);
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.showBookmarks = showBookmarks;
        init();

    }

    public HomePageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mInflater = LayoutInflater.from(context);
        init();
    }

    public HomePageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = LayoutInflater.from(context);
        init();
    }

    public boolean areBookmarksShown()
    {
        return showBookmarks;
    }

    public void init() {
        View v = mInflater.inflate(R.layout.homepage, this, true);
        ButterKnife.bind(this);
        HomePageFragmentAdapter adapter = new HomePageFragmentAdapter(((AppCompatActivity) context).getSupportFragmentManager());
        vpHomepage.setAdapter(adapter);
        tlHomepage.setupWithViewPager(vpHomepage);
        vpHomepage.setCurrentItem(showBookmarks ? HOMEPAGE_BOOKMARKS : HOMEPAGE_TOP_SITES);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    private class HomePageFragmentAdapter extends FragmentStatePagerAdapter {
        public HomePageFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case HOMEPAGE_TOP_SITES:
                    TopSitesFragment topSitesFragment = new TopSitesFragment();
                    return topSitesFragment;
                case HOMEPAGE_BOOKMARKS:
                    BookmarksFragment bookmarksFragment2 = new BookmarksFragment();
                    final Bundle bookmarksFragmentArguments2 = new Bundle();
                    bookmarksFragmentArguments2.putBoolean(BookmarksFragment.INCOGNITO_MODE, false);
                    bookmarksFragment2.setArguments(bookmarksFragmentArguments2);
                    return bookmarksFragment2;
                default:
                    throw new UnsupportedOperationException("Unsupported fragment position");
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case HOMEPAGE_TOP_SITES:
                    return context.getString(R.string.top_sites);
                case HOMEPAGE_BOOKMARKS:
                    return context.getString(R.string.action_bookmarks);
                default:
                    throw new UnsupportedOperationException("Unsupported fragment position title.");
            }
        }
    }

}
